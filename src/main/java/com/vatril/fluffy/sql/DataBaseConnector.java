package com.vatril.fluffy.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import sx.blah.discord.handle.obj.IGuild;

public class DataBaseConnector {
	
	private String username;
	private String password;
	private static DataBaseConnector dataBaseConnector;
	
	private DataBaseConnector(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
	
	public static DataBaseConnector init(String username, String password){
		if(dataBaseConnector == null){
			return (dataBaseConnector = new DataBaseConnector(username, password));
		}else{
			return dataBaseConnector;
		}
	}
	
	public void initGuildIfNotInitialized(IGuild guild){
		try {
			Class.forName("org.postgresql.Driver");
			Connection c = DriverManager.getConnection("jdbc:postgresql://localhost:5432/FluffyBot", username, password);
			c.setAutoCommit(false);
			Statement st = c.createStatement();
			ResultSet resultSet = st.executeQuery("SELECT * FROM public.guilds;");
			boolean guildadded = false;
			while(resultSet.next()){
				if((guild.getLongID() + "").equals(resultSet.getString(1))) guildadded = true;
			}
			st.close();
			if(!guildadded){
				st = c.createStatement();
				st.executeUpdate("INSERT INTO public.guilds VALUES ('" + guild.getLongID() +
						"', + '" + guild.getGeneralChannel().getLongID() + "',NULL,'//')");
				st.close();
			}
			//TODO add users
			c.commit();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}		
	}

	public static DataBaseConnector getInstance() {
		return dataBaseConnector;		
	}
}
