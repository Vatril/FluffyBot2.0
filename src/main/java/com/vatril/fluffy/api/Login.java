package com.vatril.fluffy.api;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.vatril.fluffy.util.UserInfoGetter;

/**
 * Servlet implementation class Login
 */
@WebServlet("/api/login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Login() {

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json");
		String token = request.getParameter("access_token");
		PrintWriter pr = response.getWriter();

		if (token != null) {
			UserInfoGetter uiGetter = new UserInfoGetter(token);
			request.getSession().setAttribute("userinfo", uiGetter);
			if(uiGetter.isSuccess()){
				 pr.print("{\"status\":\"success\"}");
				 return;
			}
		}
		pr.print("{\"status\":\"error\"}");
	}

}
