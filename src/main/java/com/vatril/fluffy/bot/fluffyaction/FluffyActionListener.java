package com.vatril.fluffy.bot.fluffyaction;

import com.vatril.fluffy.bot.FluffyBot;

import sx.blah.discord.api.events.Event;
import sx.blah.discord.handle.impl.events.ReadyEvent;
import sx.blah.discord.handle.impl.events.guild.AllUsersReceivedEvent;
import sx.blah.discord.handle.impl.events.guild.GuildCreateEvent;
import sx.blah.discord.handle.impl.events.guild.GuildEmojisUpdateEvent;
import sx.blah.discord.handle.impl.events.guild.GuildLeaveEvent;
import sx.blah.discord.handle.impl.events.guild.GuildTransferOwnershipEvent;
import sx.blah.discord.handle.impl.events.guild.GuildUnavailableEvent;
import sx.blah.discord.handle.impl.events.guild.GuildUpdateEvent;
import sx.blah.discord.handle.impl.events.guild.channel.ChannelCreateEvent;
import sx.blah.discord.handle.impl.events.guild.channel.ChannelDeleteEvent;
import sx.blah.discord.handle.impl.events.guild.channel.TypingEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MentionEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageDeleteEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageEmbedEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessagePinEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageSendEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageUnpinEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageUpdateEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.reaction.ReactionAddEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.reaction.ReactionRemoveEvent;
import sx.blah.discord.handle.impl.events.guild.channel.webhook.WebhookCreateEvent;
import sx.blah.discord.handle.impl.events.guild.channel.webhook.WebhookDeleteEvent;
import sx.blah.discord.handle.impl.events.guild.channel.webhook.WebhookUpdateEvent;
import sx.blah.discord.handle.impl.events.guild.member.NicknameChangedEvent;
import sx.blah.discord.handle.impl.events.guild.member.UserBanEvent;
import sx.blah.discord.handle.impl.events.guild.member.UserJoinEvent;
import sx.blah.discord.handle.impl.events.guild.member.UserLeaveEvent;
import sx.blah.discord.handle.impl.events.guild.member.UserPardonEvent;
import sx.blah.discord.handle.impl.events.guild.member.UserRoleUpdateEvent;
import sx.blah.discord.handle.impl.events.guild.role.RoleCreateEvent;
import sx.blah.discord.handle.impl.events.guild.role.RoleDeleteEvent;
import sx.blah.discord.handle.impl.events.guild.role.RoleUpdateEvent;
import sx.blah.discord.handle.impl.events.guild.voice.VoiceChannelCreateEvent;
import sx.blah.discord.handle.impl.events.guild.voice.VoiceChannelDeleteEvent;
import sx.blah.discord.handle.impl.events.guild.voice.VoicePingEvent;
import sx.blah.discord.handle.impl.events.guild.voice.user.UserSpeakingEvent;
import sx.blah.discord.handle.impl.events.guild.voice.user.UserVoiceChannelJoinEvent;
import sx.blah.discord.handle.impl.events.guild.voice.user.UserVoiceChannelLeaveEvent;
import sx.blah.discord.handle.impl.events.guild.voice.user.UserVoiceChannelMoveEvent;
import sx.blah.discord.handle.impl.events.shard.DisconnectedEvent;
import sx.blah.discord.handle.impl.events.shard.ReconnectFailureEvent;
import sx.blah.discord.handle.impl.events.shard.ReconnectSuccessEvent;
import sx.blah.discord.handle.impl.events.shard.ResumedEvent;
import sx.blah.discord.handle.impl.events.shard.ShardReadyEvent;
import sx.blah.discord.handle.impl.events.user.PresenceUpdateEvent;
import sx.blah.discord.handle.impl.events.user.UserUpdateEvent;

public abstract class FluffyActionListener {
	
	private FluffyBot fluffybot;
	private String prefix = "//";
	
	public FluffyActionListener(FluffyBot fluffyBot, String prefix){
		this.fluffybot = fluffyBot;
		this.prefix = prefix;
	}
	
	protected FluffyBot getFluffyBot(){
		return this.fluffybot;
	}
		
	public String getPrefix() {
		return prefix;
	}

	public boolean onEvent(Event event){
		return false;
	} 
	
	public boolean onAllUsersReceived(AllUsersReceivedEvent event){
		return false;
	}
	
	public boolean onChannelCreate(ChannelCreateEvent event){
		return false;
	}
	
	public boolean onChannelDelete(ChannelDeleteEvent event){
		return false;
	}
	
	public boolean onDisconect(DisconnectedEvent event){
		return false;
	}
	
	public boolean onGuildCreate(GuildCreateEvent event){
		return false;
	}
	
	public boolean onEmoji(GuildEmojisUpdateEvent event){
		return false;
	}
	
	public boolean onGuildLeave(GuildLeaveEvent event){
		return false;
	}
	
	public boolean onGuildOwnerTransfer(GuildTransferOwnershipEvent event){
		return false;
	}
	
	public boolean onGuildUnavailable(GuildUnavailableEvent event){
		return false;
	}
	
	public boolean onGuildUpdate(GuildUpdateEvent event){
		return false;
	}
	
	public boolean onMention(MentionEvent event){
		return false;
	}
	
	public boolean onMessageDeleted(MessageDeleteEvent event){
		return false;
	}
	
	public boolean onMessageEmbed(MessageEmbedEvent event){
		return false;
	}
	
	public boolean onMessagePin(MessagePinEvent event){
		return false;
	}
	
	public boolean onMessageReceived(MessageReceivedEvent event){
		return false;
	}
	
	public boolean onMessageSend(MessageSendEvent event){
		return false;
	}
	
	public boolean onMessageUnpin(MessageUnpinEvent event){
		return false;
	}
	
	public boolean onMessageUpdate(MessageUpdateEvent event){
		return false;
	}
	
	public boolean onNicknameChanged(NicknameChangedEvent event){
		return false;
	}
	
	public boolean onPresenceUpdate(PresenceUpdateEvent event){
		return false;
	}
	
	public boolean onReactionAdd(ReactionAddEvent event){
		return false;
	}
	
	public boolean onReactionRemove(ReactionRemoveEvent event){
		return false;
	}
	
	public boolean onReady(ReadyEvent event){
		return false;
	}
	
	public boolean onReconnectFailure(ReconnectFailureEvent event){
		return false;
	}
	
	public boolean onReconnectSuccess(ReconnectSuccessEvent event){
		return false;
	}
	
	public boolean onResumed(ResumedEvent event){
		return false;
	}
	
	public boolean onRoleCreated(RoleCreateEvent event){
		return false;
	}
	
	public boolean onRoleDeleted(RoleDeleteEvent event){
		return false;
	}
	
	public boolean onRoleUpdate(RoleUpdateEvent event){
		return false;
	}
	
	public boolean onShardReady(ShardReadyEvent event){
		return false;
	}
	
	public boolean onTyping(TypingEvent event){
		return false;
	}
	
	public boolean onUserBan(UserBanEvent event){
		return false;
	}
	
	public boolean onUserJoin(UserJoinEvent event){
		return false;
	}
	
	public boolean onUserLeave(UserLeaveEvent event){
		return false;
	}
	
	public boolean onUserPardon(UserPardonEvent event){
		return false;
	}
	
	public boolean onUserRoleUpdate(UserRoleUpdateEvent event){
		return false;
	}
	
	public boolean onUserUpdate(UserUpdateEvent event){
		return false;
	}
	
	public boolean onUserVoiceChannelJoin(UserVoiceChannelJoinEvent event){
		return false;
	}
	
	public boolean onUserVoiceChannelLeave(UserVoiceChannelLeaveEvent event){
		return false;
	}
	
	public boolean onUserVoiceChannelMove(UserVoiceChannelMoveEvent event){
		return false;
	}
	
	public boolean onUserVoiceChannelSpeaking(UserSpeakingEvent event){
		return false;
	}
	
	public boolean onVoiceChannelCreate(VoiceChannelCreateEvent event){
		return false;
	}
	
	public boolean onVoiceChannelDelete(VoiceChannelDeleteEvent event){
		return false;
	}
	
	public boolean onVoicePing(VoicePingEvent event){
		return false;
	}
	
	public boolean onWebhookCreate(WebhookCreateEvent event){
		return false;
	}
	
	public boolean onWebhookDelete(WebhookDeleteEvent event){
		return false;
	}
	
	public boolean onWebhookUpdate(WebhookUpdateEvent event){
		return false;
	}
	
	public boolean onSelfMessage(MessageSendEvent event) {
		return false;
	}
}
