package com.vatril.fluffy.bot.fluffyaction;

import java.util.Timer;
import java.util.TimerTask;

import com.vatril.fluffy.bot.FluffyBot;

public abstract class FluffyShortTimeListener {

	private long endtime;
	private FluffyBot fluffyBot;
	
	public FluffyShortTimeListener(FluffyBot fluffyBot, long endsIn){
		this.endtime = endsIn + System.currentTimeMillis();
		Timer timer = new Timer();
		timer.schedule(new TimerTask() {
			
			@Override
			public void run() {
				onTimeUp();	
			}
		}, endsIn);
	}
	
	protected abstract void onTimeUp();

	public long getEndtime() {
		return endtime;
	}

	public FluffyBot getFluffyBot() {
		return fluffyBot;
	}
	
}
