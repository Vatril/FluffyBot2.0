package com.vatril.fluffy.bot.fluffyaction.vatril;

import java.time.ZoneId;

import com.vatril.fluffy.bot.FluffyBot;
import com.vatril.fluffy.bot.annotation.FluffyAction;
import com.vatril.fluffy.bot.annotation.SelfAction;
import com.vatril.fluffy.bot.fluffyaction.FluffyActionListener;

import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageSendEvent;
import sx.blah.discord.handle.obj.IMessage;

@SelfAction
@FluffyAction
public class Ping extends FluffyActionListener {

	public Ping(FluffyBot fluffyBot, String prefix) {
		super(fluffyBot, prefix);
	}

	@Override
	public boolean onMessageReceived(MessageReceivedEvent event) {
		return ping(event.getMessage());
	}

	@Override
	public boolean onSelfMessage(MessageSendEvent event) {
		return ping(event.getMessage());
	}

	private boolean ping(IMessage message) {
		if (message.getContent().startsWith(getPrefix() + "ping")) {
			message.reply("Pong " + (System.currentTimeMillis()
					- message.getTimestamp().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()) + "ms");

			return true;
		}

		return false;
	}
}
