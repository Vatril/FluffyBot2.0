package com.vatril.fluffy.bot.fluffyaction.vatril;

import com.vatril.fluffy.bot.FluffyBot;
import com.vatril.fluffy.bot.annotation.FluffyAction;
import com.vatril.fluffy.bot.fluffyaction.FluffyActionListener;
import com.vatril.fluffy.bot.fluffyaction.FluffyTemporaryAction;

import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

@FluffyAction
public class Vote extends FluffyActionListener {

	public Vote(FluffyBot fluffyBot, String prefix) {
		super(fluffyBot, prefix);
	}

	@Override
	public boolean onMessageReceived(MessageReceivedEvent event) {

		if (event.getMessage().getContent().startsWith("//vote")) {

			getFluffyBot().addTemporaryListener(new QuestionAsker(getFluffyBot(), event.getAuthor().getLongID(),
					event.getGuild().getLongID(), event.getChannel().getLongID()));
			event.getMessage().reply("Please enter your question:");
			return true;
		}

		return false;
	}

	private class QuestionAsker extends FluffyTemporaryAction {

		long ochannel;
		long oauthor;
		long oguild;

		public QuestionAsker(FluffyBot fluffyBot, long author, long guild, long channel) {
			super(fluffyBot, null);
			this.oauthor = author;
			this.ochannel = channel;
			this.oguild = guild;
		}

		@Override
		public boolean onMessageReceived(MessageReceivedEvent event) {
			if (event.getAuthor().getLongID() == oauthor && oguild == event.getGuild().getLongID()
					&& ochannel == event.getChannel().getLongID()) {
				String votemessage = event.getMessage().getContent();
				event.getMessage().reply("Is that correct? [__y__es __n__o __e__xit]");

				getFluffyBot().addTemporaryListener(
						new QuestionCorrectAsker(getFluffyBot(), oauthor, oguild, ochannel, votemessage));
				remove();
				return true;
			}
			return false;
		}
	}

	private class QuestionCorrectAsker extends FluffyTemporaryAction {

		long ochannel;
		long oauthor;
		long oguild;
		String votemessage;

		public QuestionCorrectAsker(FluffyBot fluffyBot, long author, long guild, long channel, String votemessage) {
			super(fluffyBot, null);
			this.oauthor = author;
			this.ochannel = channel;
			this.oguild = guild;
			this.votemessage = votemessage;
		}

		@Override
		public boolean onMessageReceived(MessageReceivedEvent event) {
			if (event.getAuthor().getLongID() == oauthor && oguild == event.getGuild().getLongID()
					&& ochannel == event.getChannel().getLongID()) {
				if (event.getMessage().getContent().startsWith("y")) {
					event.getChannel().sendMessage(":thumbsup: :thumbsdown: " + votemessage);
					remove();
				} else if (event.getMessage().getContent().startsWith("n")) {
					getFluffyBot().addTemporaryListener(new QuestionAsker(getFluffyBot(), oauthor, oguild, ochannel));
					event.getMessage().reply("Please enter your question:");
					remove();
				} else if (event.getMessage().getContent().startsWith("e")) {
					event.getMessage().reply("Ok, canceling");
					remove();
				} else {
					event.getMessage().reply("Please reply with __y__es __n__o or __e__xit");
				}
				return true;
			}
			return false;
		}
	}

}
