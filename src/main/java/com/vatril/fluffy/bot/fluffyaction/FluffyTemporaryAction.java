package com.vatril.fluffy.bot.fluffyaction;


import com.vatril.fluffy.bot.FluffyBot;

public class FluffyTemporaryAction extends FluffyActionListener {
	

	public FluffyTemporaryAction(FluffyBot fluffyBot, String prefix) {
		super(fluffyBot, prefix);
	}
	
	protected void remove(){
		getFluffyBot().removeTemporaryListener(this);
	}

}
