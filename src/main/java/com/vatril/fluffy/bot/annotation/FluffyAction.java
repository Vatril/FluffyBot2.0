package com.vatril.fluffy.bot.annotation;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface FluffyAction {

	public int value() default 0;
}
