package com.vatril.fluffy.bot;

import java.util.ArrayList;
import java.util.Set;

import org.reflections.Reflections;

import com.vatril.fluffy.bot.annotation.FluffyAction;
import com.vatril.fluffy.bot.fluffyaction.FluffyActionListener;

public class ReflectionsLoader {
	
	private ArrayList<Class <FluffyActionListener>> fluffyactionclasses;

	
	public ReflectionsLoader(){
		
		fluffyactionclasses = new ArrayList<>();
		
		Reflections reflections = new Reflections("com.vatril.fluffy.bot");
		
		Set<Class<?>> fluffyactions = reflections.getTypesAnnotatedWith(FluffyAction.class);
		
		
		
		fluffyactions.forEach(fa -> {
			if(FluffyActionListener.class.isAssignableFrom(fa)){
				fluffyactionclasses.add((Class<FluffyActionListener>) fa);
				System.out.println("added " + fa.getName());
			}
		});
		
		
		
		
	}


	public ArrayList<Class<FluffyActionListener>> getFluffyactionclasses() {
		return fluffyactionclasses;
	}
}
