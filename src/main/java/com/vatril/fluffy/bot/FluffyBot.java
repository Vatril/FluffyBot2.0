package com.vatril.fluffy.bot;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;

import com.vatril.fluffy.bot.annotation.FluffyAction;
import com.vatril.fluffy.bot.fluffyaction.FluffyActionListener;
import com.vatril.fluffy.bot.fluffyaction.FluffyTemporaryAction;
import com.vatril.fluffy.sql.DataBaseConnector;

import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.events.Event;
import sx.blah.discord.api.events.EventDispatcher;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.ReadyEvent;
import sx.blah.discord.handle.impl.events.guild.AllUsersReceivedEvent;
import sx.blah.discord.handle.impl.events.guild.channel.ChannelCreateEvent;
import sx.blah.discord.handle.impl.events.guild.channel.ChannelDeleteEvent;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.handle.impl.events.guild.GuildCreateEvent;
import sx.blah.discord.handle.impl.events.guild.GuildEmojisUpdateEvent;
import sx.blah.discord.handle.impl.events.guild.GuildLeaveEvent;
import sx.blah.discord.handle.impl.events.guild.GuildTransferOwnershipEvent;
import sx.blah.discord.handle.impl.events.guild.GuildUnavailableEvent;
import sx.blah.discord.handle.impl.events.guild.GuildUpdateEvent;
import sx.blah.discord.handle.impl.events.guild.channel.TypingEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MentionEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageDeleteEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageEmbedEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessagePinEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageSendEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageUnpinEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageUpdateEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.reaction.ReactionAddEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.reaction.ReactionRemoveEvent;
import sx.blah.discord.handle.impl.events.guild.channel.webhook.WebhookCreateEvent;
import sx.blah.discord.handle.impl.events.guild.channel.webhook.WebhookDeleteEvent;
import sx.blah.discord.handle.impl.events.guild.channel.webhook.WebhookUpdateEvent;
import sx.blah.discord.handle.impl.events.guild.member.NicknameChangedEvent;
import sx.blah.discord.handle.impl.events.guild.member.UserBanEvent;
import sx.blah.discord.handle.impl.events.guild.member.UserJoinEvent;
import sx.blah.discord.handle.impl.events.guild.member.UserLeaveEvent;
import sx.blah.discord.handle.impl.events.guild.member.UserPardonEvent;
import sx.blah.discord.handle.impl.events.guild.member.UserRoleUpdateEvent;
import sx.blah.discord.handle.impl.events.guild.role.RoleCreateEvent;
import sx.blah.discord.handle.impl.events.guild.role.RoleDeleteEvent;
import sx.blah.discord.handle.impl.events.guild.role.RoleUpdateEvent;
import sx.blah.discord.handle.impl.events.guild.voice.VoiceChannelCreateEvent;
import sx.blah.discord.handle.impl.events.guild.voice.VoiceChannelDeleteEvent;
import sx.blah.discord.handle.impl.events.guild.voice.VoicePingEvent;
import sx.blah.discord.handle.impl.events.guild.voice.user.UserSpeakingEvent;
import sx.blah.discord.handle.impl.events.guild.voice.user.UserVoiceChannelJoinEvent;
import sx.blah.discord.handle.impl.events.guild.voice.user.UserVoiceChannelLeaveEvent;
import sx.blah.discord.handle.impl.events.guild.voice.user.UserVoiceChannelMoveEvent;
import sx.blah.discord.handle.impl.events.shard.DisconnectedEvent;
import sx.blah.discord.handle.impl.events.shard.ReconnectFailureEvent;
import sx.blah.discord.handle.impl.events.shard.ReconnectSuccessEvent;
import sx.blah.discord.handle.impl.events.shard.ResumedEvent;
import sx.blah.discord.handle.impl.events.shard.ShardReadyEvent;
import sx.blah.discord.handle.impl.events.user.PresenceUpdateEvent;
import sx.blah.discord.handle.impl.events.user.UserUpdateEvent;
import sx.blah.discord.handle.obj.IGuild;

public class FluffyBot {

	private boolean hasLoggedIn;
	private IDiscordClient client;
	private HashSet<FluffyTemporaryAction> temporaryListeners;

	public FluffyBot(String token) {
		this(token, new ArrayList<Class<FluffyActionListener>>());
	}

	public FluffyBot(String token, ArrayList<Class<FluffyActionListener>> fluffyactions) {
		temporaryListeners = new HashSet<>();
		ClientBuilder clientBuilder = new ClientBuilder();
		clientBuilder.withToken(token);

		try {

			client = clientBuilder.login();
			EventDispatcher dispatcher = client.getDispatcher();

			dispatcher.registerListener(new IListener<ReadyEvent>() {

				@Override
				public void handle(ReadyEvent event) {
					hasLoggedIn = true;
					DataBaseConnector dbc = DataBaseConnector.getInstance();
					Iterator<IGuild> git = client.getGuilds().iterator();
					while (git.hasNext()) {
						dbc.initGuildIfNotInitialized(git.next());
					}
				}

			});

			FluffyBot fb = this;

			dispatcher.registerListener(new IListener<Event>() {

				@Override
				public void handle(Event event) {
					Thread t = new Thread(new Runnable() {

						@Override
						public void run() {
							ArrayList<FluffyActionListener> flisteners = new ArrayList<>();
							for (Class<FluffyActionListener> fclass : fluffyactions) {
								try {
									FluffyActionListener fa = fclass.getDeclaredConstructor(FluffyBot.class, String.class)
											.newInstance(fb,"//");
									flisteners.add(fa);
								} catch (Exception e) {
									e.printStackTrace();
								}
							}

							Collections.sort(flisteners, new Comparator<FluffyActionListener>() {

								@Override
								public int compare(FluffyActionListener o1, FluffyActionListener o2) {
									try {
										int a1 = o1.getClass().getAnnotation(FluffyAction.class).value();
										int a2 = o2.getClass().getAnnotation(FluffyAction.class).value();
										return (a1 == a2) ? 0 : (a1 > a2) ? 1 : -1;
									} catch (Exception e) {
										e.printStackTrace();
									}
									return 0;
								}

							});

							Iterator<FluffyTemporaryAction> it = temporaryListeners.iterator();

							while (it.hasNext()) {
								flisteners.add(0, it.next());
							}

							for (FluffyActionListener fal : flisteners) {
								try {
									if (event instanceof MessageSendEvent) {
										MessageSendEvent mevt = (MessageSendEvent) event;
										if (!mevt.getAuthor().isBot()
												&& mevt.getMessage().getContent().startsWith("self.")
												&& fal.onSelfMessage(mevt))
											break;
									}
									if (event instanceof AllUsersReceivedEvent) {
										if (fal.onAllUsersReceived((AllUsersReceivedEvent) event))
											break;
									}
									if (event instanceof ChannelCreateEvent) {
										if (fal.onChannelCreate((ChannelCreateEvent) event))
											break;
									}
									if (event instanceof ChannelDeleteEvent) {
										if (fal.onChannelDelete((ChannelDeleteEvent) event))
											break;
									}
									if (event instanceof DisconnectedEvent) {
										if (fal.onDisconect((DisconnectedEvent) event))
											break;
									}
									if (event instanceof GuildCreateEvent) {
										if (fal.onGuildCreate((GuildCreateEvent) event))
											break;
									}
									if (event instanceof GuildEmojisUpdateEvent) {
										if (fal.onEmoji((GuildEmojisUpdateEvent) event))
											break;
									}
									if (event instanceof GuildLeaveEvent) {
										if (fal.onGuildLeave((GuildLeaveEvent) event))
											break;
									}
									if (event instanceof GuildTransferOwnershipEvent) {
										if (fal.onGuildOwnerTransfer((GuildTransferOwnershipEvent) event))
											break;
									}
									if (event instanceof GuildUnavailableEvent) {
										if (fal.onGuildUnavailable((GuildUnavailableEvent) event))
											break;
									}
									if (event instanceof GuildUpdateEvent) {
										if (fal.onGuildUpdate((GuildUpdateEvent) event))
											break;
									}
									if (event instanceof MentionEvent) {
										if (fal.onMention((MentionEvent) event))
											break;
									}
									if (event instanceof MessageDeleteEvent) {
										if (fal.onMessageDeleted((MessageDeleteEvent) event))
											break;
									}
									if (event instanceof MessageEmbedEvent) {
										if (fal.onMessageEmbed((MessageEmbedEvent) event))
											break;
									}
									if (event instanceof MessageReceivedEvent) {
										if (fal.onMessageReceived((MessageReceivedEvent) event))
											break;
									}
									if (event instanceof MessagePinEvent) {
										if (fal.onMessagePin((MessagePinEvent) event))
											break;
									}
									if (event instanceof MessageSendEvent) {
										if (fal.onMessageSend((MessageSendEvent) event))
											break;
									}
									if (event instanceof MessageUnpinEvent) {
										if (fal.onMessageUnpin((MessageUnpinEvent) event))
											break;
									}
									if (event instanceof MessageUpdateEvent) {
										if (fal.onMessageUpdate((MessageUpdateEvent) event))
											break;
									}
									if (event instanceof NicknameChangedEvent) {
										if (fal.onNicknameChanged((NicknameChangedEvent) event))
											break;
									}
									if (event instanceof PresenceUpdateEvent) {
										if (fal.onPresenceUpdate((PresenceUpdateEvent) event))
											break;
									}
									if (event instanceof ReactionAddEvent) {
										if (fal.onReactionAdd((ReactionAddEvent) event))
											break;
									}
									if (event instanceof ReactionRemoveEvent) {
										if (fal.onReactionRemove((ReactionRemoveEvent) event))
											break;
									}
									if (event instanceof ReadyEvent) {
										if (fal.onReady((ReadyEvent) event))
											break;
									}
									if (event instanceof ReconnectFailureEvent) {
										if (fal.onReconnectFailure((ReconnectFailureEvent) event))
											break;
									}
									if (event instanceof ReconnectSuccessEvent) {
										if (fal.onReconnectSuccess((ReconnectSuccessEvent) event))
											break;
									}
									if (event instanceof ResumedEvent) {
										if (fal.onResumed((ResumedEvent) event))
											break;
									}
									if (event instanceof RoleCreateEvent) {
										if (fal.onRoleCreated((RoleCreateEvent) event))
											break;
									}
									if (event instanceof RoleDeleteEvent) {
										if (fal.onRoleDeleted((RoleDeleteEvent) event))
											break;
									}
									if (event instanceof RoleUpdateEvent) {
										if (fal.onRoleUpdate((RoleUpdateEvent) event))
											break;
									}
									if (event instanceof ShardReadyEvent) {
										if (fal.onShardReady((ShardReadyEvent) event))
											break;
									}
									if (event instanceof TypingEvent) {
										if (fal.onTyping((TypingEvent) event))
											break;
									}
									if (event instanceof UserBanEvent) {
										if (fal.onUserBan((UserBanEvent) event))
											break;
									}
									if (event instanceof UserJoinEvent) {
										if (fal.onUserJoin((UserJoinEvent) event))
											break;
									}
									if (event instanceof UserLeaveEvent) {
										if (fal.onUserLeave((UserLeaveEvent) event))
											break;
									}
									if (event instanceof UserPardonEvent) {
										if (fal.onUserPardon((UserPardonEvent) event))
											break;
									}
									if (event instanceof UserRoleUpdateEvent) {
										if (fal.onUserRoleUpdate((UserRoleUpdateEvent) event))
											break;
									}
									if (event instanceof UserUpdateEvent) {
										if (fal.onUserUpdate((UserUpdateEvent) event))
											break;
									}
									if (event instanceof UserVoiceChannelJoinEvent) {
										if (fal.onUserVoiceChannelJoin((UserVoiceChannelJoinEvent) event))
											break;
									}
									if (event instanceof UserVoiceChannelLeaveEvent) {
										if (fal.onUserVoiceChannelLeave((UserVoiceChannelLeaveEvent) event))
											break;
									}
									if (event instanceof UserVoiceChannelMoveEvent) {
										if (fal.onUserVoiceChannelMove((UserVoiceChannelMoveEvent) event))
											break;
									}
									if (event instanceof UserSpeakingEvent) {
										if (fal.onUserVoiceChannelSpeaking((UserSpeakingEvent) event))
											break;
									}
									if (event instanceof VoiceChannelCreateEvent) {
										if (fal.onVoiceChannelCreate((VoiceChannelCreateEvent) event))
											break;
									}
									if (event instanceof VoiceChannelDeleteEvent) {
										if (fal.onVoiceChannelDelete((VoiceChannelDeleteEvent) event))
											break;
									}
									if (event instanceof VoicePingEvent) {
										if (fal.onVoicePing((VoicePingEvent) event))
											break;
									}
									if (event instanceof WebhookCreateEvent) {
										if (fal.onWebhookCreate((WebhookCreateEvent) event))
											break;
									}
									if (event instanceof WebhookDeleteEvent) {
										if (fal.onWebhookDelete((WebhookDeleteEvent) event))
											break;
									}
									if (event instanceof WebhookUpdateEvent) {
										if (fal.onWebhookUpdate((WebhookUpdateEvent) event))
											break;
									}

									if (fal.onEvent(event))
										break;
								} catch (Exception e) {
									e.printStackTrace();
								}
							}

						}
					});
					t.start();
				}

			});

		} catch (DiscordException e) {
			e.printStackTrace();
		}

	}

	public void logout() {
		if (client != null) {
			client.logout();
		}
	}

	public boolean isHasLoggedIn() {
		return hasLoggedIn;
	}

	public IDiscordClient getClient() {
		return client;
	}

	public void addTemporaryListener(FluffyTemporaryAction fluffyTemporaryAction) {
		temporaryListeners.add(fluffyTemporaryAction);
	}

	public void removeTemporaryListener(FluffyTemporaryAction fluffyTemporaryAction) {
		temporaryListeners.remove(fluffyTemporaryAction);
	}

}
