package com.vatril.fluffy.web.listener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.json.JSONObject;

import com.vatril.fluffy.bot.FluffyBot;
import com.vatril.fluffy.bot.ReflectionsLoader;
import com.vatril.fluffy.sql.DataBaseConnector;

/**
 * Application Lifecycle Listener implementation class InitListener
 *
 */
@WebListener
public class InitListener implements ServletContextListener {

	/**
	 * Default constructor.
	 */
	public InitListener() {
	}

	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent sce) {
		FluffyBot bot = (FluffyBot) sce.getServletContext().getAttribute("bot");
		if (bot != null) {
			bot.logout();
		}
	}

	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent sce) {
		ReflectionsLoader loader = new ReflectionsLoader();
		BufferedReader reader = new BufferedReader(
				new InputStreamReader(sce.getServletContext().getResourceAsStream("WEB-INF/token")));
		FluffyBot bot;
		try {
			String jsonTokens = "";
			String line;
			while((line = reader.readLine())!=null){
				jsonTokens += line;
			}
			JSONObject jsonObject = new JSONObject(jsonTokens);
			
			DataBaseConnector.init(jsonObject.getString("psqluser"), jsonObject.getString("password"));
			
			bot = new FluffyBot(jsonObject.getString("token"),loader.getFluffyactionclasses());
			sce.getServletContext().setAttribute("bot", bot);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
