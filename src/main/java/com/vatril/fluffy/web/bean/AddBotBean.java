package com.vatril.fluffy.web.bean;



import javax.faces.bean.ManagedBean;


@ManagedBean
public class AddBotBean {

	private String addlink = "https://discordapp.com/oauth2/authorize?client_id=313002316355862528&scope=bot&permissions=8";
	private String addAsSelfbot = "https://discordapp.com/oauth2/authorize?client_id=313002316355862528&scope=rpc.api&response_type=token";
	private String loginlink= "https://discordapp.com/oauth2/authorize?client_id=313002316355862528&scope=identify&response_type=token";

	public String getAddlink() {
		return addlink;
	}

	public void setAddlink(String addlink) {
		this.addlink = addlink;
	}

	public String getLoginlink() {
		return loginlink;
	}

	public void setLoginlink(String loginlink) {
		this.loginlink = loginlink;
	}

	public String getAddAsSelfbot() {
		return addAsSelfbot;
	}

	public void setAddAsSelfbot(String addAsSelfbot) {
		this.addAsSelfbot = addAsSelfbot;
	}
	
	
	
}
