package com.vatril.fluffy.web.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import com.vatril.fluffy.util.UserInfoGetter;

@ManagedBean
public class TestDashboardBean {

	private String username;
	private String imageurl;
	
	public TestDashboardBean(){
		try{
		UserInfoGetter uiGetter = (UserInfoGetter) ((HttpSession)FacesContext.getCurrentInstance().getExternalContext().getSession(true)).getAttribute("userinfo");
		this.username = uiGetter.getUsername();
		this.imageurl = uiGetter.getAvatarLink(1024);
		} catch (NullPointerException | ClassCastException e) {
			e.printStackTrace();
		}
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getImageurl() {
		return imageurl;
	}
	public void setImageurl(String imageurl) {
		this.imageurl = imageurl;
	}
	
}
