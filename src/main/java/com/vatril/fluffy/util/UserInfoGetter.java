package com.vatril.fluffy.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

public class UserInfoGetter {

	private String token;
	private String username;
	private String discriminator;
	private boolean mfa_enabled;
	private String id;
	private String avatar;
	private boolean success;
	private String error;
	
	
	public UserInfoGetter(String token){
		this.token = token;
		
		HttpURLConnection conn = null;
		try {
			URL u = new URL("https://discordapp.com/api/users/@me");
			conn = (HttpURLConnection) u.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("User-Agent", "DiscordBot");
			conn.setRequestProperty("Authorization", "Bearer " + token);

			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));

			String res = "";
			String line;
			while ((line = reader.readLine()) != null) {
				res += line;
			}

			System.out.println(res);

			JSONObject json = new JSONObject(res);
			
			username = json.getString("username");
			discriminator = json.getString("discriminator");
			id = json.getString("id");
			avatar = json.getString("avatar");
			mfa_enabled = json.getBoolean("mfa_enabled");
			
			success = true;

		} catch (IOException | JSONException e) {
			success = false;
			try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getErrorStream()));

			String res = "";
			String line;
			while ((line = reader.readLine()) != null) {
				res += line;
			}
			System.err.println(res);
			error = res;
			}catch (NullPointerException | IOException ex) {
				ex.printStackTrace();
			}
			e.printStackTrace();
		}
	}


	public String getToken() {
		return token;
	}


	public String getUsername() {
		return username;
	}


	public String getDiscriminator() {
		return discriminator;
	}


	public boolean isMfa_enabled() {
		return mfa_enabled;
	}


	public String getId() {
		return id;
	}


	public String getAvatar() {
		return avatar;
	}


	public boolean isSuccess() {
		return success;
	}


	public String getError() {
		return error;
	}
	
	
	public String getAvatarLink(int size, String format){
		return "https://cdn.discordapp.com/avatars/" + getId() + "/" + getAvatar() + "." + format + ((size != 0)?"?size=" + size:"");
	}
	
	public String getAvatarLink(String format){
		return getAvatarLink(0, format);
	}
	
	public String getAvatarLink(int size){
		return getAvatarLink(size,"png");
	}
	
	public String getAvatarLink(){
		return getAvatarLink(0,"png");
	}
}
